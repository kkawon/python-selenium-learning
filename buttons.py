from enum import Enum
from selenium.webdriver.common.by import By


class Buttons(Enum):
    # buttons : basic numbers
    ZERO_BUTTON = (By.XPATH, "//button[@id='Btn0']")
    ONE_BUTTON = (By.XPATH, "//button[@id='Btn1']")
    TWO_BUTTON = (By.XPATH, "//button[@id='Btn2']")
    THREE_BUTTON = (By.XPATH, "//button[@id='Btn3']")
    FOUR_BUTTON = (By.XPATH, "//button[@id='Btn4']")
    FIVE_BUTTON = (By.XPATH, "//button[@id='Btn5']")
    SIX_BUTTON = (By.XPATH, "//button[@id='Btn6']")
    SEVEN_BUTTON = (By.XPATH, "//button[@id='Btn7']")
    EIGHT_BUTTON = (By.XPATH, "//button[@id='Btn8']")
    NINE_BUTTON = (By.XPATH, "//button[@id='Btn9']")
    # buttons : functions
    RAD_BUTTON = (By.XPATH, "//input[@id='trigorad']")
    POPUP_BUTTON = (By.XPATH, "//button[@aria-label='Consent']")
    CLEAR_BUTTON = (By.XPATH, "//*[@id='BtnClear']")
    COS_BUTTON = (By.XPATH, "//button[@id='BtnCos']")
    PI_BUTTON = (By.XPATH, "//button[@id='BtnPi']")
    SUM_BUTTON = (By.XPATH, "//button[@id='BtnCalc']")
    HISTORY_DROPDOWN_BUTTON = (By.XPATH, "//button[@class='btn dropdown-toggle pull-right']")


import logging
import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.webdriver import WebDriver
from pages.graph_page import GraphPage

logging.basicConfig(level=logging.INFO)
log = logging.getLogger('CalculatorLogger')


@pytest.fixture
def graph():
    log.info('SETUP START')
    driver: WebDriver
    driver = webdriver.Chrome(ChromeDriverManager().install())
    fun_graph = GraphPage(driver)
    fun_graph.set_up()
    log.info('SETUP END')
    yield fun_graph
    fun_graph.tear_down()


def test_graph(graph):
    graph.fill_input('cos(x)')

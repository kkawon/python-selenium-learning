import logging
import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.webdriver import WebDriver
from pages.calculator_page import CalculatorPage
from buttons import Buttons
from fraction import Fraction

logging.basicConfig(level=logging.INFO)
log = logging.getLogger('CalculatorLogger')


@pytest.fixture
def calc():
    log.info('SETUP START')
    driver: WebDriver
    driver = webdriver.Chrome(ChromeDriverManager().install())
    fun_calc = CalculatorPage(driver)
    driver.get('http://web2.0calc.com/')
    driver.find_element(*Buttons.POPUP_BUTTON.value).click()
    log.info('SETUP END')
    yield fun_calc
    driver.quit()


def test_calc(calc):

    # test steps
    calc.fill_input('35*999+(100/4)')
    first_result = calc.get_result()
    calc.clear_calculator().\
        click_element(Buttons.RAD_BUTTON).\
        click_element(Buttons.COS_BUTTON).\
        click_element(Buttons.PI_BUTTON).\
        calculate()
    second_result = calc.get_result()
    calc.clear_calculator().\
        fill_input('sqrt(81)')
    third_result = calc.get_result()
    history_results = calc.get_history()

    # assertions
    log.info(f'result 1 was:{first_result}, '
             f'result 2 was:{second_result}, '
             f'result 3 was:{third_result}')
    assert (first_result, second_result, third_result) == ('34990', '-1', '9')
    assert history_results == [f'{third_result}', 'sqrt(81)',
                               f'{second_result}', 'cos(pi',
                               f"{first_result}", '35*999+(100/4)']


def test_calc_second(calc):

    # test steps
    calc.fill_input('10*10%')
    result = calc.get_result()

    # assertions
    log.info(f'result 1 was:{result}')
    assert result == '1'


def test_calc_third(calc):

    # test steps
    calc.fill_input('2 + 2')
    result = calc.get_result()

    # assertions
    assert result == '4'


# test: fractions


def test_fractions_by_input(calc):

    # test steps
    first_fraction = Fraction(1, 1, 2)
    second_fraction = Fraction(1, 1, 2)
    calc.go_to_fractions()\
        .fill_fractions_fields(first_fraction, second_fraction)
    calc.calculate()
    result = calc.get_result()

    # assertions
    log.info(f'result was:{result}')
    assert result == '3'


def test_fractions_by_buttons(calc):

    # test steps
    calc.go_to_fractions()
    calc.fraction_component.click_fractions_element(0, Buttons.ONE_BUTTON)
    calc.fraction_component.click_fractions_element(1, Buttons.ONE_BUTTON)
    calc.fraction_component.click_fractions_element(2, Buttons.TWO_BUTTON)
    calc.fraction_component.click_fractions_element(3, Buttons.ONE_BUTTON)
    calc.fraction_component.click_fractions_element(4, Buttons.ONE_BUTTON)
    calc.fraction_component.click_fractions_element(5, Buttons.TWO_BUTTON)
    calc.calculate()
    result = calc.get_result()

    # assertions
    log.info(f'result was:{result}')
    assert result == '3'

# test: main menu


def test_homepage_by_button(calc):

    # test steps
    calc.go_to_fractions()
    calc.go_to_homepage()
    result = calc.get_url()

    # assertions
    log.info(f'www is:{result}')
    assert result == 'https://web2.0calc.com/'

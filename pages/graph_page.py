from selenium.webdriver.common.by import By
from pages.base_page import BasePage
from selenium.webdriver.common.keys import Keys
import logging

logging.basicConfig(level=logging.INFO)
log = logging.getLogger('CalculatorLogger')

# locators
function_field_var = (By.XPATH, "//var[@mathquill-command-id]")
function_field_span = (By.XPATH, "//span[@mathquill-command-id]")
function_field = (By.XPATH, "//input[@class='text inp']")


class GraphPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)

    def set_up(self):
        self.driver.get('https://web2.0calc.com/graphs/6752250d5d39/')
        function_input = self.driver.find_element(*function_field)
        function_input.send_keys(Keys.BACKSPACE)
        # element_list = self.driver.find_elements(*function_field_var)
        # element2_list = self.driver.find_elements(*function_field_span)
        # for element in element_list:
        #     element.clear()
        # for element2 in element2_list:
        #     element2.clear()

    def tear_down(self):
        self.driver.quit()


    def fill_input(self, equation):
        log.info(f'provided equation was:{equation}')
        element = self.driver.find_element(*function_field)
        element.send_keys(equation)
        element.send_keys(Keys.ENTER)


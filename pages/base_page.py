from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
import logging

logging.basicConfig(level=logging.INFO)
log = logging.getLogger('CalculatorLogger')


class BasePage:
    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 10)

    def wait_for_presence(self, selector):  # tez do utilsow
        log.info(f'used selector was:{selector}')
        self.wait.until(ec.presence_of_element_located(selector))

    def click_element(self, button):  # przerzucic do utils
        log.info(f'used selector was:{button}')
        element = self.driver.find_element(*button.value)
        element.click()
        return self

    def get_current_url(self):
        url = self.driver.current_url
        return url

from selenium.webdriver.common.by import By
from buttons import Buttons
from components.fraction_component import FractionComponent
from components.main_menu_component import MainMenuComponent
from selenium.webdriver.support import expected_conditions as ec
from pages.base_page import BasePage
import logging

logging.basicConfig(level=logging.INFO)
log = logging.getLogger('CalculatorLogger')

# locators
home_button = (By.XPATH, "//li[@id='menuhome']")
input_helper = (By.XPATH, "//div[@id='inputhelper']")
fractions_helper = (By.XPATH, "//*[@class='fraction']")
fractions_fields = (By.XPATH, "//input[@class='form-control text']")
wait_element = (By.XPATH, "//input[@class='disable1d']")
input_field = (By.XPATH, "//input[@id='input']")
history_result = (By.XPATH, "//div[@id ='histframe']//p")


class CalculatorPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)
        self.fraction_component = FractionComponent(driver)
        self.main_menu_component = MainMenuComponent(driver)

    def wait_for_result(self, selector, equation):
        log.info(f'used selector was:{selector} and result was:{equation}')
        self.wait.until_not(ec.text_to_be_present_in_element_value(selector, equation))

    def fill_input(self, equation):
        log.info(f'provided equation was:{equation}')
        element = self.driver.find_element(*input_field)
        element.clear()
        element.send_keys(equation)
        element = self.driver.find_element(*Buttons.SUM_BUTTON.value)
        element.click()
        self.wait_for_result(input_field, equation)
        return self

    def calculate(self):
        element = self.driver.find_element(*Buttons.SUM_BUTTON.value)
        element.click()
        self.wait_for_presence(wait_element)
        return self

    def clear_calculator(self):
        element = self.driver.find_element(*Buttons.CLEAR_BUTTON.value)
        element.click()
        return self

    def get_result(self):
        element = self.driver.find_element(*input_field)
        result = element.get_attribute('value')
        return result

    @staticmethod
    def get_attribute_of_elem(elem):
        return elem.get_attribute('title')

    def get_history(self):
        element = self.driver.find_element(*Buttons.HISTORY_DROPDOWN_BUTTON.value)
        element.click()
        hist_res = self.driver.find_elements(*history_result)
        result = map(self.get_attribute_of_elem, hist_res)
        return list(result)

    # methods : fractions

    def go_to_fractions(self):
        element = self.driver.find_element(*input_helper)
        element.click()
        element_fraction = self.driver.find_element(*fractions_helper)
        element_fraction.click()
        self.wait_for_presence(fractions_fields)
        return self.fraction_component

    def go_to_homepage(self):
        self.main_menu_component.click_home_button()

    def get_url(self):
        url = self.get_current_url()
        return url

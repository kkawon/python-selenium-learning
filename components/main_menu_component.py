from selenium.webdriver.common.by import By
import logging

logging.basicConfig(level=logging.INFO)
log = logging.getLogger('MainMenuLogger')

home_button = (By.XPATH, "//li[@id='menuhome']")
forum_button = (By.XPATH, "//li[@id='menuforum']")
formulary_dropdown = (By.XPATH, "//li[@id='menuformulary']")
help_dropdown = (By.XPATH, "//li[@id='menuhelp']")
about_dropdown = (By.XPATH, "//li[@title='About']")


class MainMenuComponent:
    def __init__(self, driver):
        self.driver = driver

    def click_home_button(self):
        home_btn = self.driver.find_element(*home_button)
        home_btn.click()

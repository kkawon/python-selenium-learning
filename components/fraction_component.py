from selenium.webdriver.common.by import By
import logging

logging.basicConfig(level=logging.INFO)
log = logging.getLogger('FractionLogger')

# locators
fractions_fields = (By.XPATH, "//input[@class='form-control text']")


class FractionComponent:
    def __init__(self, driver):
        self.driver = driver

    def fill_fractions_fields(self, first_fraction, second_fraction):
        frac_results = self.driver.find_elements(*fractions_fields)
        frac_results[0].send_keys(first_fraction.integer)
        frac_results[1].send_keys(first_fraction.top)
        frac_results[2].send_keys(first_fraction.bottom)
        frac_results[3].send_keys(second_fraction.integer)
        frac_results[4].send_keys(second_fraction.top)
        frac_results[5].send_keys(second_fraction.bottom)

    def move_to_fraction_field(self, num_of_element):
        frac_results = self.driver.find_elements(*fractions_fields)
        frac_results[num_of_element].click()

    def click_fractions_element(self, num_of_element, button):
        frac_results = self.driver.find_elements(*fractions_fields)
        frac_results[num_of_element].click()
        log.info(f'used selector was:{button}')
        element = self.driver.find_element(*button.value)
        element.click()
